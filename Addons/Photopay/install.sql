CREATE TABLE IF NOT EXISTS wp_photopay_video(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10)  unsigned NOT NULL DEFAULT 0 COMMENT '用户ID',
  `type` int(1) unsigned NOT NULL DEFAULT 0 COMMENT '视频类型',
  `title` char(200) NOT NULL DEFAULT '' COMMENT '标题',
  `img`  char(250) not null default '' comment '图片地址',
  `src`  char(250) not null default '' comment '视频原地址',
  `info` text   comment '描述',
  `status` int(1) not null default 0 comment '状态',
  `updated_time` int(10) not null default 0 comment '修改时间',
  `created_time` int(10) not null default 0 comment '添加时间',
   PRIMARY KEY (`id`)
)ENGINE=MyISAM DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci CHECKSUM=0 ROW_FORMAT=DYNAMIC DELAY_KEY_WRITE=0;

CREATE TABLE IF NOT EXISTS wp_photopay_type(
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
   `name` char(100) not null default '' comment '分类名称',
   `status` int(1) not null default 0 comment '状态',
   `updated_time` int(10) not null default 0 comment '修改时间',
  `created_time` int(10) not null default 0 comment '添加时间',
   PRIMARY KEY (`id`)
)ENGINE=MyISAM DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci CHECKSUM=0 ROW_FORMAT=DYNAMIC DELAY_KEY_WRITE=0;


