<?php

namespace Addons\Photopay;
use Common\Controller\Addon;

/**
 * 打赏看视频插件
 * @author jwf
 */

    class PhotopayAddon extends Addon{

        public $info = array(
            'name'=>'Photopay',
            'title'=>'打赏看视频',
            'description'=>'打赏看视屏',
            'status'=>1,
            'author'=>'jwf',
            'version'=>'0.1',
            'has_adminlist'=>0
        );

		public function install() {
			$install_sql = './Addons/Photopay/install.sql';
			if (file_exists ( $install_sql )) {
				execute_sql_file ( $install_sql );
			}
			return true;
		}
		public function uninstall() {
			$uninstall_sql = './Addons/Photopay/uninstall.sql';
			if (file_exists ( $uninstall_sql )) {
				execute_sql_file ( $uninstall_sql );
			}
			return true;
		}

        //实现的weixin钩子方法
        public function weixin($param){

        }

 

}