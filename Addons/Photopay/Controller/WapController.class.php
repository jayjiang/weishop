<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Addons\Photopay\Controller;

/**
 * Description of WapController
 *
 * @author SFB
 */
class WapController extends \Think\WapBaseController{
  

  //put your code here
  /**
   * 
   */
  public function index(){
	$token = session ( 'token' );
	$old_openid = session ( 'openid_' . $token );
	$this->display();
  }
  
  public function video(){
	$this->display();
  }
  
  public function weipay(){
	header ( "Content-type: text/html; charset=utf-8" );
	// token
	$token = get_token ();
	// 微信用户ID
	$openid = get_openid ();
	// 订单名称
	$orderName = urlencode ( "我是测试的订单" );
	// 订单ID
	$orderid = date ( 'Ymd' ) . substr ( implode ( NULL, array_map ( 'ord', str_split ( substr ( uniqid (), 7, 13 ), 1 ) ) ), 0, 8 );
	// 支付金额
	$price = 0.1;
	// 支付类型
	$zftype = 0;
	/*
	 * 成功后返回调用的方法 addons_url的格式
	 * 返回GET参数:token,wecha_id,orderid
	 * 以下用playok的方法来说明，其实这个地址也是由开发者随意定的
	 */
	$from = "Payment:__Payment_playok";
	$bid = "";
	$sid = "";
	$url = addons_url ( 'Payment://Weixin/pay', array (
			'from' => $from,
			'orderName' => $orderName,
			'price' => $price,
			'token' => $token,
			'wecha_id' => $openid,
			'paytype' => $zftype,
			'orderNumber' => $orderid,
			'bid' => $bid,
			'sid' => $sid 
	) );
	redirect ( $url, 1, '您好,准备跳转到支付页面,请不要重复刷新页面,请耐心等待...' );
  }
  
}
